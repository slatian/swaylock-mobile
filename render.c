#include <math.h>
#include <stdlib.h>
#include <wayland-client.h>
#include "cairo.h"
#include "background-image.h"
#include "swaylock.h"

#define M_PI 3.14159265358979323846
const float TYPE_INDICATOR_RANGE = M_PI / 3.0f;

static void set_color_for_state(cairo_t *cairo, struct swaylock_state *state,
		struct swaylock_colorset *colorset) {
	if (state->auth_state == AUTH_STATE_VALIDATING) {
		cairo_set_source_u32(cairo, colorset->verifying);
	} else if (state->auth_state == AUTH_STATE_INVALID) {
		cairo_set_source_u32(cairo, colorset->wrong);
	} else if (state->auth_state == AUTH_STATE_CLEAR) {
		cairo_set_source_u32(cairo, colorset->cleared);
	} else {
		if (state->xkb.caps_lock && state->args.show_caps_lock_indicator) {
			cairo_set_source_u32(cairo, colorset->caps_lock);
		} else if (state->xkb.caps_lock && !state->args.show_caps_lock_indicator &&
				state->args.show_caps_lock_text) {
			uint32_t inputtextcolor = state->args.colors.text.input;
			state->args.colors.text.input = state->args.colors.text.caps_lock;
			cairo_set_source_u32(cairo, colorset->input);
			state->args.colors.text.input = inputtextcolor;
		} else {
			cairo_set_source_u32(cairo, colorset->input);
		}
	}
}

void draw_boxed_text(cairo_t *cairo, struct swaylock_state *state,
		char *text, int pos_x, int pos_y, int width, int height,
		struct swaylock_colorset *colorset_text,
		struct swaylock_colorset *colorset_background) {
	set_color_for_state(cairo, state, colorset_background);
	cairo_rectangle(cairo, pos_x, pos_y, width, height);
	cairo_fill(cairo);
	//Draw text
	cairo_text_extents_t extents;
	cairo_font_extents_t fe;
	set_color_for_state(cairo, state, colorset_text);
	cairo_text_extents(cairo, text, &extents);
	cairo_font_extents(cairo, &fe);
	cairo_move_to(cairo, pos_x + (width - extents.width) / 2, pos_y + height / 2 + (fe.height / 2 - fe.descent));
	cairo_show_text(cairo, text);
}

void render_frame_background(struct swaylock_surface *surface) {
	struct swaylock_state *state = surface->state;

	int buffer_width = surface->width * surface->scale;
	int buffer_height = surface->height * surface->scale;
	if (buffer_width == 0 || buffer_height == 0) {
		return; // not yet configured
	}

	surface->current_buffer = get_next_buffer(state->shm,
			surface->buffers, buffer_width, buffer_height);
	if (surface->current_buffer == NULL) {
		return;
	}

	cairo_t *cairo = surface->current_buffer->cairo;
	cairo_set_antialias(cairo, CAIRO_ANTIALIAS_BEST);

	cairo_save(cairo);
	cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
	cairo_set_source_u32(cairo, state->args.colors.background);
	cairo_paint(cairo);
	if (surface->image && state->args.mode != BACKGROUND_MODE_SOLID_COLOR) {
		cairo_set_operator(cairo, CAIRO_OPERATOR_OVER);
		render_background_image(cairo, surface->image,
			state->args.mode, buffer_width, buffer_height);
	}
	cairo_restore(cairo);
	cairo_identity_matrix(cairo);

	wl_surface_set_buffer_scale(surface->surface, surface->scale);
	wl_surface_attach(surface->surface, surface->current_buffer->buffer, 0, 0);
	wl_surface_damage_buffer(surface->surface, 0, 0, INT32_MAX, INT32_MAX);
	wl_surface_commit(surface->surface);
}

void render_indicator_frame(struct swaylock_surface *surface) {
	struct swaylock_state *state = surface->state;

	int ind_radius = state->args.radius * surface->scale;
	int ind_thickness = state->args.thickness * surface->scale;
	int margin = state->args.margin * surface->scale;
	
	int parent_width = surface->width * surface->scale;
	int parent_height = surface->height * surface->scale;
	
	if (state->args.style == INDICATOR_STYLE_RING) {
		if ((ind_radius*2 + margin*2 + ind_thickness*2) > parent_width) {
			ind_radius = parent_width/2 - margin - ind_thickness;
		}
		
		if ((ind_radius*2 + margin*2 + ind_thickness*2) > parent_height) {
			ind_radius = parent_height/2 - margin - ind_thickness;
		}
	} else {
		if ((ind_radius*2 + margin*2) > parent_width) {
			ind_radius = parent_width/2 - margin;
		}
	}
	
	int buffer_diameter = (ind_radius + ind_thickness) * 2;

	int buffer_width = surface->indicator_width;
	int buffer_height = surface->indicator_height;
	int new_width = buffer_diameter;
	int new_height = buffer_diameter;
	int indicator_y_offset = 0;
	
	if (state->args.style == INDICATOR_STYLE_FLAT) {
		new_height = ind_thickness;
		new_width = ind_radius*2;
	}
	
	double box_padding = 4.0 * surface->scale;
	float border_width = 2.0 * surface->scale;
	
	int subsurf_xpos;
	int subsurf_ypos;

	// Center the indicator unless overridden by the user
	if (state->args.override_indicator_x_position) {
		subsurf_xpos = state->args.indicator_x_position -
			buffer_width / (2 * surface->scale) + 2 / surface->scale;
	} else {
		subsurf_xpos = surface->width / 2 -
			buffer_width / (2 * surface->scale) + 2 / surface->scale;
	}

	if (state->args.override_indicator_y_position) {
		subsurf_ypos = state->args.indicator_y_position -
			buffer_height / (2 * surface->scale) + 2 / surface->scale;
	} else {
		subsurf_ypos = surface->height / 2 -
			buffer_height / (2 * surface->scale) + 2 / surface->scale;
	}
	
	if (subsurf_xpos < margin) subsurf_xpos = margin;
	if (subsurf_ypos < margin) subsurf_ypos = margin;

	wl_subsurface_set_position(surface->indicator_subsurface,
			subsurf_xpos / surface->scale, subsurf_ypos / surface->scale);

	surface->current_buffer = get_next_buffer(state->shm,
			surface->indicator_buffers, buffer_width, buffer_height);
	if (surface->current_buffer == NULL) {
		return;
	}

	// Hide subsurface until we want it visible
	wl_surface_attach(surface->indicator_child, NULL, 0, 0);
	wl_surface_commit(surface->indicator_child);

	cairo_t *cairo = surface->current_buffer->cairo;
	cairo_set_antialias(cairo, CAIRO_ANTIALIAS_BEST);
	cairo_font_options_t *fo = cairo_font_options_create();
	cairo_font_options_set_hint_style(fo, CAIRO_HINT_STYLE_FULL);
	cairo_font_options_set_antialias(fo, CAIRO_ANTIALIAS_SUBPIXEL);
	cairo_font_options_set_subpixel_order(fo, to_cairo_subpixel_order(surface->subpixel));
	cairo_set_font_options(cairo, fo);
	cairo_font_options_destroy(fo);
	cairo_identity_matrix(cairo);

	// Clear
	cairo_save(cairo);
	cairo_set_source_rgba(cairo, 0, 0, 0, 0);
	cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cairo);
	cairo_restore(cairo);

	if (state->args.style && (state->auth_state != AUTH_STATE_IDLE ||
			state->args.indicator_idle_visible)) {
		if (state->args.style == INDICATOR_STYLE_FLAT) {
			// Draw background
			cairo_set_line_width(cairo, 0);
			cairo_rectangle(cairo, 0, 0, buffer_width, buffer_height);
			set_color_for_state(cairo, state, &state->args.colors.inside);
			cairo_fill_preserve(cairo);
			cairo_stroke(cairo);
			// Draw Indicator
			cairo_rectangle(cairo, 0, indicator_y_offset, buffer_width, ind_thickness);
			set_color_for_state(cairo, state, &state->args.colors.ring);
			cairo_fill_preserve(cairo);
			cairo_stroke(cairo);

		} else {
			// Fall back to the ring style
			// Fill inner circle
			cairo_set_line_width(cairo, 0);
			cairo_arc(cairo, buffer_width / 2, buffer_diameter / 2,
					ind_radius - ind_thickness / 2, 0, 2 * M_PI);
			set_color_for_state(cairo, state, &state->args.colors.inside);
			cairo_fill_preserve(cairo);
			cairo_stroke(cairo);

			// Draw ring
			cairo_set_line_width(cairo, ind_thickness);
			cairo_arc(cairo, buffer_width / 2, buffer_diameter / 2, ind_radius,
					0, 2 * M_PI);
			set_color_for_state(cairo, state, &state->args.colors.ring);
			cairo_stroke(cairo);
		}

		// Draw a message
		char *text = NULL;
		const char *layout_text = NULL;
		char attempts[4]; // like i3lock: count no more than 999
		set_color_for_state(cairo, state, &state->args.colors.text);
		cairo_select_font_face(cairo, state->args.font,
				CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
		if (state->args.font_size > 0) {
			cairo_set_font_size(cairo, state->args.font_size);
		} else {
			cairo_set_font_size(cairo, ind_radius / 3.0f);
		}
		switch (state->auth_state) {
		case AUTH_STATE_VALIDATING:
			text = "verifying";
			break;
		case AUTH_STATE_INVALID:
			text = "wrong";
			break;
		case AUTH_STATE_CLEAR:
			text = "cleared";
			break;
		case AUTH_STATE_INPUT:
		case AUTH_STATE_INPUT_NOP:
		case AUTH_STATE_BACKSPACE:
			// Caps Lock has higher priority
			if (state->xkb.caps_lock && state->args.show_caps_lock_text) {
				text = "Caps Lock";
			} else if (state->args.show_failed_attempts &&
					state->failed_attempts > 0) {
				if (state->failed_attempts > 999) {
					text = "999+";
				} else {
					snprintf(attempts, sizeof(attempts), "%d", state->failed_attempts);
					text = attempts;
				}
			}

			xkb_layout_index_t num_layout = xkb_keymap_num_layouts(state->xkb.keymap);
			if (!state->args.hide_keyboard_layout &&
					(state->args.show_keyboard_layout || num_layout > 1)) {
				xkb_layout_index_t curr_layout = 0;

				// advance to the first active layout (if any)
				while (curr_layout < num_layout &&
					xkb_state_layout_index_is_active(state->xkb.state,
						curr_layout, XKB_STATE_LAYOUT_EFFECTIVE) != 1) {
					++curr_layout;
				}
				// will handle invalid index if none are active
				layout_text = xkb_keymap_layout_get_name(state->xkb.keymap, curr_layout);
			}
			break;
		default:
			break;
		}
		
		double text_width = 0;

		if (text) {
			cairo_text_extents_t extents;
			cairo_font_extents_t fe;
			double x, y;
			cairo_text_extents(cairo, text, &extents);
			cairo_font_extents(cairo, &fe);
			if (state->args.style == INDICATOR_STYLE_FLAT) {
				x = box_padding + border_width;
				y = indicator_y_offset + ind_thickness + box_padding + (fe.height - fe.descent);
				new_height += box_padding * 2 + fe.height + 2 * border_width;
			} else {
				x = (buffer_width / 2) -
					(extents.width / 2 + extents.x_bearing);
				y = (buffer_diameter / 2) +
					(fe.height / 2 - fe.descent);
			}

			cairo_move_to(cairo, x, y);
			cairo_show_text(cairo, text);
			cairo_close_path(cairo);
			cairo_new_sub_path(cairo);

			text_width = extents.width + 2 * border_width + 2 * box_padding;
			
			if (new_width < text_width) {
				new_width = text_width;
			}
			
		}

		// Typing indicator: Highlight random part on keypress
		if (state->auth_state == AUTH_STATE_INPUT
					|| state->auth_state == AUTH_STATE_BACKSPACE) {
			if (state->args.style == INDICATOR_STYLE_FLAT) {
				//Map width to straight line
				int highlight_width = (ind_radius*TYPE_INDICATOR_RANGE/M_PI);
				double highlight_start = 0;
				highlight_start +=
					(rand() % (int)(buffer_width - highlight_width - border_width * 2));
				cairo_rectangle(cairo, highlight_start, indicator_y_offset, highlight_width, ind_thickness);
				if (state->auth_state == AUTH_STATE_INPUT) {
					if (state->xkb.caps_lock && state->args.show_caps_lock_indicator) {
						cairo_set_source_u32(cairo, state->args.colors.caps_lock_key_highlight);
					} else {
						cairo_set_source_u32(cairo, state->args.colors.key_highlight);
					}
				} else {
					if (state->xkb.caps_lock && state->args.show_caps_lock_indicator) {
						cairo_set_source_u32(cairo, state->args.colors.caps_lock_bs_highlight);
					} else {
						cairo_set_source_u32(cairo, state->args.colors.bs_highlight);
					}
				}
				cairo_fill_preserve(cairo);
				
				// Draw borders
				cairo_set_source_u32(cairo, state->args.colors.separator);
				cairo_set_line_width(cairo, 2.0 * surface->scale);
				cairo_stroke(cairo);
			} else {
				// Fall back to the ring style
				static double highlight_start = 0;
				highlight_start +=
					(rand() % (int)(M_PI * 100)) / 100.0 + M_PI * 0.5;
				cairo_arc(cairo, buffer_width / 2, buffer_diameter / 2,
						ind_radius, highlight_start,
						highlight_start + TYPE_INDICATOR_RANGE);
				if (state->auth_state == AUTH_STATE_INPUT) {
					if (state->xkb.caps_lock && state->args.show_caps_lock_indicator) {
						cairo_set_source_u32(cairo, state->args.colors.caps_lock_key_highlight);
					} else {
						cairo_set_source_u32(cairo, state->args.colors.key_highlight);
					}
				} else {
					if (state->xkb.caps_lock && state->args.show_caps_lock_indicator) {
						cairo_set_source_u32(cairo, state->args.colors.caps_lock_bs_highlight);
					} else {
						cairo_set_source_u32(cairo, state->args.colors.bs_highlight);
					}
				}
				cairo_stroke(cairo);

				// Draw borders
				double border_rad_width = (border_width / (ind_radius*2*M_PI)) * 2*M_PI;
				cairo_set_source_u32(cairo, state->args.colors.separator);
				cairo_arc(cairo, buffer_width / 2, buffer_diameter / 2,
						ind_radius, highlight_start,
						highlight_start + border_rad_width);
				cairo_stroke(cairo);

				cairo_arc(cairo, buffer_width / 2, buffer_diameter / 2,
						ind_radius, highlight_start + TYPE_INDICATOR_RANGE,
						highlight_start + TYPE_INDICATOR_RANGE +
							border_rad_width);
				cairo_stroke(cairo);
			}
		}
		
		//Draw borders
		set_color_for_state(cairo, state, &state->args.colors.line);
		cairo_set_line_width(cairo, 2.0 * surface->scale);
		if (state->args.style == INDICATOR_STYLE_FLAT) {
			// Draw outer border of the box
			cairo_rectangle(cairo, surface->scale, surface->scale, buffer_width-(surface->scale*2), buffer_height-(surface->scale*2));
			cairo_stroke(cairo);
			cairo_rectangle(cairo, surface->scale, indicator_y_offset, buffer_width-(surface->scale*2), ind_thickness);
			cairo_stroke(cairo);
		} else {
			// Fall back to the ring style
			// Draw inner + outer border of the circle
			cairo_arc(cairo, buffer_width / 2, buffer_diameter / 2,
					ind_radius - ind_thickness / 2, 0, 2 * M_PI);
			cairo_stroke(cairo);
			cairo_arc(cairo, buffer_width / 2, buffer_diameter / 2,
					ind_radius + ind_thickness / 2, 0, 2 * M_PI);
			cairo_stroke(cairo);
		}

		// display layout text separately
		if (layout_text) {
			cairo_text_extents_t extents;
			cairo_font_extents_t fe;
			double x, y;
			cairo_text_extents(cairo, layout_text, &extents);
			cairo_font_extents(cairo, &fe);
			if (state->args.style == INDICATOR_STYLE_FLAT) {
				x = buffer_width - extents.width - box_padding * 3 - border_width;
				y = indicator_y_offset + ind_thickness + box_padding / 2 + border_width;
			} else {
				// upper left coordinates for box
				x = (buffer_width / 2) - (extents.width / 2) - box_padding;
				y = buffer_diameter;
			}

			// background box
			cairo_rectangle(cairo, x, y,
				extents.width + box_padding * 2,
				fe.height + box_padding);
			cairo_set_source_u32(cairo, state->args.colors.layout_background);
			cairo_fill_preserve(cairo);
			// border
			cairo_set_source_u32(cairo, state->args.colors.layout_border);
			cairo_stroke(cairo);

			// take font extents and padding into account
			cairo_move_to(cairo,
				x - extents.x_bearing + box_padding,
				y + (fe.height - fe.descent));
			cairo_set_source_u32(cairo, state->args.colors.layout_text);
			cairo_show_text(cairo, layout_text);
			cairo_close_path(cairo);
			cairo_new_sub_path(cairo);

			if (state->args.style == INDICATOR_STYLE_RING) {
				new_height += fe.height + 2 * box_padding;
			}
			
			//Hack to detect if the height was already expanded by the previous text
			if (text_width == 0) {
				new_height += box_padding * 2 + fe.height + 2 * border_width;
			}
		
			text_width += extents.width + 2 * box_padding;
			
			if (new_width < text_width) {
				new_width = text_width;
			}
		}
	}
	
	if ((uint32_t)(new_width + margin*2) > (surface->width * surface->scale)) {
		new_width = surface->width * surface->scale - margin*2;
	}
	
	if ((uint32_t)(new_height + margin*2) > (surface->height * surface->scale)) {
		new_height = surface->height * surface->scale - margin*2;
	}
	
	// Ensure buffer size is multiple of buffer scale - required by protocol
	if (new_height % surface->scale) new_height += surface->scale - (new_height % surface->scale);
	if (new_width % surface->scale) new_width += surface->scale - (new_width % surface->scale);

	if (buffer_width != new_width || buffer_height != new_height) {
		destroy_buffer(surface->current_buffer);
		surface->indicator_width = new_width;
		surface->indicator_height = new_height;
		wl_surface_damage_buffer(surface->surface, subsurf_xpos, subsurf_ypos, buffer_width * surface->scale, buffer_height * surface->scale);
		render_indicator_frame(surface);
		return;
	}

	wl_surface_set_buffer_scale(surface->indicator_child, surface->scale);
	wl_surface_attach(surface->indicator_child, surface->current_buffer->buffer, 0, 0);
	wl_surface_damage_buffer(surface->indicator_child, 0, 0, INT32_MAX, INT32_MAX);
	wl_surface_commit(surface->indicator_child);

	wl_surface_commit(surface->surface);
}

void render_keypad_frame(struct swaylock_surface *surface) {
	struct swaylock_state *state = surface->state;

	int buffer_width = surface->keypad_width;
	int buffer_height = surface->keypad_height;
	
	int subsurf_xpos = state->args.margin;
	int subsurf_ypos = surface->height - (buffer_height / surface->scale);
	
	int horizontal_padding = 8.0 * surface->scale;
	int vertical_padding = 4.0 * surface->scale;
	
	int spacing = 4.0 * surface->scale;
	int margin = state->args.margin * surface->scale;
	
	subsurf_ypos -= margin;
	
	int new_width = surface->width * surface->scale; //buffer_width;
	int new_height = buffer_height;
	
	wl_subsurface_set_position(surface->keypad_subsurface, subsurf_xpos, subsurf_ypos);
	
	surface->current_buffer = get_next_buffer(state->shm,
			surface->keypad_buffers, buffer_width, buffer_height);
	if (surface->current_buffer == NULL) {
		return;
	}
	
	// Hide subsurface until we want it visible
	wl_surface_attach(surface->keypad_child, NULL, 0, 0);
	wl_surface_commit(surface->keypad_child);
	
	cairo_t *cairo = surface->current_buffer->cairo;
	cairo_set_antialias(cairo, CAIRO_ANTIALIAS_BEST);
	cairo_font_options_t *fo = cairo_font_options_create();
	cairo_font_options_set_hint_style(fo, CAIRO_HINT_STYLE_FULL);
	cairo_font_options_set_antialias(fo, CAIRO_ANTIALIAS_SUBPIXEL);
	cairo_font_options_set_subpixel_order(fo, to_cairo_subpixel_order(surface->subpixel));
	cairo_set_font_options(cairo, fo);
	cairo_font_options_destroy(fo);
	cairo_identity_matrix(cairo);

	// Clear
	cairo_save(cairo);
	cairo_set_source_rgba(cairo, 0, 0, 0, 0);
	cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cairo);
	cairo_restore(cairo);
	
	cairo_select_font_face(cairo, state->args.font,
			CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
	if (state->args.font_size > 0) {
		cairo_set_font_size(cairo, state->args.font_size);
	} else {
		cairo_set_font_size(cairo, surface->scale * 16);
	}
	
	cairo_text_extents_t extents;
	cairo_font_extents_t fe;
	cairo_text_extents(cairo, "000", &extents);
	cairo_font_extents(cairo, &fe);
	
	int keypad_min_width = extents.width * 3 + horizontal_padding * 6 + spacing * 4;
	if (buffer_width < keypad_min_width) {
		new_width = keypad_min_width;
	}
	
	int keypad_min_height = fe.height * 5 + vertical_padding * 10 + spacing * 6;
	if (buffer_height < keypad_min_height) {
		new_height = keypad_min_height;
	}
	
	int key_width = (buffer_width - spacing * 4) / 3;
	int key_height = (buffer_height - spacing * 6) / 5;
	
	char label[2];
	
	label[1] = '\0';
	
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 3; x++) {
			int pos_x = spacing * (x+1) + key_width * x;
			int pos_y = spacing * (y+1) + key_height * y;
			//Draw key label
			label[0] = '1' + y * 3 + x;
			if (label[0] == ';') {
				label[0] = '0';
			} else if (label[0] == ':') {
				label[0] = ' ';
			}
			draw_boxed_text(cairo, state, label, pos_x, pos_y, key_width, key_height,
				&state->args.colors.text, &state->args.colors.inside);
		}
	}
	
	int pos_x = spacing;
	int pos_y = spacing * 5 + key_height * 4;
	draw_boxed_text(cairo, state, "Unlock", pos_x, pos_y, key_width*3+spacing*2, key_height,
				&state->args.colors.text, &state->args.colors.inside);
	
	// Make sure the keypad fits on the screen
	if ((uint32_t)(new_width + margin*2) > (surface->width * surface->scale)) {
		new_width = surface->width * surface->scale - margin*2;
	}
	
	if ((uint32_t)(new_height + margin*2) > (surface->height * surface->scale)) {
		new_height = surface->height * surface->scale - margin*2;
	}
	
	// Ensure buffer size is multiple of buffer scale - required by protocol
	if (new_height % surface->scale) new_height += surface->scale - (new_height % surface->scale);
	if (new_width % surface->scale) new_width += surface->scale - (new_width % surface->scale);

	if (buffer_width != new_width || buffer_height != new_height) {
		destroy_buffer(surface->current_buffer);
		surface->keypad_width = new_width;
		surface->keypad_height = new_height;
		wl_surface_damage_buffer(surface->surface, subsurf_xpos, subsurf_ypos, buffer_width * surface->scale, buffer_height * surface->scale);
		render_keypad_frame(surface);
		return;
	}
	
	wl_surface_set_buffer_scale(surface->keypad_child, surface->scale);
	wl_surface_attach(surface->keypad_child, surface->current_buffer->buffer, 0, 0);
	wl_surface_damage_buffer(surface->keypad_child, 0, 0, INT32_MAX, INT32_MAX);
	wl_surface_commit(surface->keypad_child);
	
	wl_surface_commit(surface->surface);
}
